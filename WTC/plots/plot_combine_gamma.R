# This file is part of the following manuscript:
# Title:   Efficient design of synthetic gene circuits under cell-to-cell variability
# Authors: Baptiste Turpin, Eline Y. Bijman, Hans-Michael Kaltenbach, Joerg Stelling*
#
# *Corresponding authors
#
# (C) Copyright 2022 ETH Zurich, D-BSSE, Baptiste Turpin
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php

source("source_files.R")
## Load params ----
# Make sure to run from the directory where `init_sample_pop.R` is
max_IDR = 150               #max input dynamic range for ref curve
variance_model = "identical" # "diagonal" or "identical"
s_threshold = NA
source("./init_sample_pop.R")

save_res_mcmc_gamma1 = "./saves/save_gamma_unif_thr6_1_sigma.rds"
save_res_mcmc_gamma2 = "./saves/save_gamma_unif_thr3_1_sigma.rds"
save_res_mcmc_gamma_fixed = "./saves/save_gamma_N7_thr6_1_sigma_fixed2.rds"

plot_2 = TRUE
plot_fixed = FALSE
plot_outliers = FALSE

## Read data ----
res_mcmc_gamma1 = readRDS(save_res_mcmc_gamma1)
n_desired_samples1 = res_mcmc_gamma1$n_plot
nsteps1 = dim(res_mcmc_gamma1$samples)[1]
burnin1 = floor(nsteps1/2)
n_temp1 = dim(res_mcmc_gamma1$samples)[2]
distrib_to_plot1 = n_temp1
res_mcmc_loc1 = res_mcmc_gamma1$samples[seq(burnin1+1, nsteps1, len = n_desired_samples1),
                                          n_temp1,]


res_mcmc_gamma2 = readRDS(save_res_mcmc_gamma2)
n_desired_samples2 = res_mcmc_gamma2$n_plot
nsteps2 = dim(res_mcmc_gamma2$samples)[1]
burnin2 = floor(nsteps2/2)
n_temp2 = dim(res_mcmc_gamma2$samples)[2]
distrib_to_plot2 = n_temp2
res_mcmc_loc2 = res_mcmc_gamma2$samples[seq(burnin2+1, nsteps2, len = n_desired_samples2),
                                          n_temp2,]

res_mcmc_gamma_fixed = readRDS(save_res_mcmc_gamma_fixed)
n_desired_samples_fixed = res_mcmc_gamma_fixed$n_plot
nsteps_fixed = dim(res_mcmc_gamma_fixed$samples)[1]
burnin_fixed = floor(nsteps_fixed/2)
n_temp_fixed = dim(res_mcmc_gamma_fixed$samples)[2]
distrib_to_plot_fixed = n_temp_fixed
res_mcmc_loc_fixed = res_mcmc_gamma_fixed$samples[seq(burnin_fixed+1, nsteps_fixed, len = n_desired_samples_fixed),
                                        n_temp_fixed,]
res_mcmc_loc_fixed = cbind(res_mcmc_loc_fixed, log10(0.07))

## Grouping data ----
res_mcmc_loc = res_mcmc_loc1
groups = rep(1, nrow(res_mcmc_loc1))

## Set different colors per group and color in black invalid populations ----
red = RColorBrewer::brewer.pal(9, 'YlOrRd')[6]
col1 = rep("orange", nrow(res_mcmc_loc1))
col1[res_mcmc_gamma1$resp_data$pop_costs > DATA$global_threshold |
       is.na(res_mcmc_gamma1$resp_data$pop_costs)] = "black"
col2 = rep("red", nrow(res_mcmc_loc2))
col2[res_mcmc_gamma2$resp_data$pop_costs > DATA$global_threshold |
       is.na(res_mcmc_gamma2$resp_data$pop_costs)] = "black"

col_fixed = rep("purple", nrow(res_mcmc_loc_fixed))
col_fixed[res_mcmc_gamma_fixed$resp_data$pop_costs > DATA$global_threshold |
       is.na(res_mcmc_gamma_fixed$resp_data$pop_costs)] = "black"

col = col1
col_small = "orange"

if (plot_2){
  res_mcmc_loc = rbind(res_mcmc_loc, res_mcmc_loc2)
  groups = c(groups, rep(2, nrow(res_mcmc_loc2)))
  col = c(col, col2)
  col_small = c(col_small, "red")
}
if (plot_fixed){
  res_mcmc_loc = rbind(res_mcmc_loc, res_mcmc_loc_fixed)
  groups = c(groups, rep(max(groups) + 1, nrow(res_mcmc_loc_fixed)))
  col = c(col, col_fixed)
  col_small = c(col_small, "purple")
}

id_keep = rep(TRUE, nrow(res_mcmc_loc))
col_final = col
if (!plot_outliers){
  id_keep = (col != "black")
  col_final = col_small
}

# Make colors transparent
# col = rgb(t(col2rgb(col)), max = 255, alpha = 80)
# col_small = rgb(t(col2rgb(col_small)), max = 255, alpha = 80)

## Plot ----
plotDistrib(res_mcmc_loc[id_keep,], 
            col = col_final,
            groups = groups[id_keep],
            lim_mins = bmin_gamma, 
            lim_maxs = bmax_gamma,
            param_names = param_names,
            cex = NULL,
            disp_hist = FALSE)






