# This file is part of the following manuscript:
# Title:   Efficient design of synthetic gene circuits under cell-to-cell variability
# Authors: Baptiste Turpin, Eline Y. Bijman, Hans-Michael Kaltenbach, Joerg Stelling*
#
# *Corresponding authors
#
# (C) Copyright 2022 ETH Zurich, D-BSSE, Baptiste Turpin
#
# Licensed under the MIT license:
#
#     http://www.opensource.org/licenses/mit-license.php


##General Parameters ----
# Parameters `variance_model` and `s_threshold` are set before in the sampling scripts.

id_mod_gamma_mu = c("d1", "d3", "theta_alpha", "theta_beta")
# id_mod_gamma_mu = c("d1", "d2", "d3", 
#                     "theta_alpha", "theta_beta",
#                     "ktet_alpha", "krnr_beta", "ktet_C",
#                     "Kd", "n")
id_mod = c("d1", "d2", "d3", "theta_alpha", "theta_beta",   #parameters modified at indiv level either through mean, var or both.
           "ktet_alpha", "krnr_beta", "ktet_C")
# id_mod = id_mod_gamma_mu
id_variable = c(1, 2, 3,  6, 7, 8)
log_transform = id_variable
N_indiv = 300

pSpecs = read.csv('paramSpecsDilution.txt')

global_threshold = 0.2


##Dependant variables ----
if (variance_model == "diagonal"){
  n_sigma = length(id_variable)
  sigma_diag = paste0("L_sigma_",  as.character(1:n_sigma), "_", as.character(1:n_sigma))
  param_names = expression(d[Tet], d[C],
                           theta[Tet], theta[Tup],
                           sigma[d[Tet]], sigma[d[Tup]], sigma[d[C]],
                           sigma[k[Tet]], sigma[k[Tup]], sigma[k[C]])
  # param_names = expression(d[Tet], d[Tup], d[C],
  #                          theta[Tet], theta[Tup],
  #                          k[Tet], k[Tup], k[C],
  #                          K[d], n,
  #                          sigma[d[Tet]], sigma[d[Tup]], sigma[d[C]],
  #                          sigma[k[Tet]], sigma[k[Tup]], sigma[k[C]])
} else{
  sigma_diag = "L_sigma_global_CV"
  param_names = expression(d[Tet], d[C],
                           theta[Tet], theta[Tup],
                           CV)
}
id_mod_gamma = c(id_mod_gamma_mu,  #parameters to modify at the pop level
                 sigma_diag)
# Fix the variability
if (exists("fix_variability") && fix_variability){
  id_mod_gamma = id_mod_gamma_mu
}

d = length(id_mod_gamma)
bmin_gamma = pSpecs$bmin[match(id_mod_gamma, pSpecs$names)]
bmin_gamma[bmin_gamma==0] = 1e-6                                   #Set a non-zero minimum for parameters (for-logspace)
bmax_gamma = pSpecs$bmax[match(id_mod_gamma, pSpecs$names)]
names(bmin_gamma) = id_mod_gamma
names(bmax_gamma) = id_mod_gamma
id_log_gamma = pSpecs$islog[match(id_mod_gamma, pSpecs$names)]
id_log = pSpecs$islog[match(id_mod, pSpecs$names)]
bmin_gamma[id_log_gamma==1] = log10(bmin_gamma[id_log_gamma==1])    #do we want to explore pop params in log-space?
bmax_gamma[id_log_gamma==1] = log10(bmax_gamma[id_log_gamma==1])

#Define start point as specified in param file
p0 = pSpecs$p0[match(id_mod_gamma, pSpecs$names)]
p0[id_log_gamma==1] = log10(p0[id_log_gamma==1])

#or define it as the best individual param
# id_p0 = which.min(y_mcmc)
# p0 = res_mcmc[id_p0,]
# p0[(length(id_mod_loc) + 1):d] = -2

#Or load it
#p0 = readRDS("p0_small_d_Tet.rds")

names(p0) = id_mod_gamma

# Get the sets of parameters that do not vary between evluations
# Get the identifiers of mean and sd parameters
id_mu = grep("L_sigma", pSpecs$names, invert = T)
params_fixed = pSpecs$fixed_value[id_mu]
names_all_params = c(pSpecs$names[id_mu], sigma_diag)
params_fixed_gamma = pSpecs$fixed_value[match(c(pSpecs$names[id_mu], sigma_diag), pSpecs$names)]
names(params_fixed_gamma) = names_all_params
id_sigma = grep("L_sigma", names_all_params, invert = F)

DATA = initDATA(N_indiv = N_indiv,                        #Number of samples for naive simulation.
                id_log = id_log,                          #which of the individual params should be in log-space for model evaluation (just a matter of convention)
                id_log_gamma = id_log_gamma,              #which of the pop        params are to be sampled in log-space (0 and 1 values)
                id_mod = id_mod,                          #which of the individual params are modified and therefore params of the GP (names); should be the same as the one used in building interpolant
                id_variable = id_variable,                #which of the individual params among id_mod are cell-cell variable (indices)
                id_mod_gamma = id_mod_gamma,              #which of the pop        params are modified (names)
                id_mu = id_mu,                            #which of the pop params represent the mean vector (indices)
                id_sigma = id_sigma,                      #which of the pop params represent the sd vector (indices)
                log_transform = log_transform,            #which of the individual params among id_mod follow a log_transform distrib (indices)
                s_threshold = s_threshold,                #individual cost threshold
                global_threshold = global_threshold,      #Admissible fraction of non-viable cells in population
                params_fixed = params_fixed,              #default values for indiv params (used in model evaluation)
                params_fixed_gamma = params_fixed_gamma,  #default values for pop params   (used in sample generation)
                variance_model = variance_model,          #model for covariance matrix, either "scalar" or "diagonal"
                tmax = 25e6,                              #time at which to evaluate steady-state by default in simulations (minutes)
                tcheck_ss = 500,                          #length of the time interval over which to assess if steady-state is reached (minutes)
                max_try_ode = 100,                        #number of possible tries (simulations) for one individual in case steady-state is not reached
                eps_tolerance_ss = 1e-10,                 #relative tolerance to assess steady-state over the time interval `tcheck_ss`
                dose = seq(0, max_IDR, by = 25),                #vector of doses (nM) where response is simulated and compared
                ref_curve = 60/150 * seq(0, max_IDR, by = 25)) #vector of responses (nM) corresponding to `dose`. Ideal response curve for an individual cell

