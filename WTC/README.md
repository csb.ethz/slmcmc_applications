# Well-tempered controller

*A posteriori* case-study on the design of a transcriptional controller meant to achieve high Input Dynamic Range (IDR) and Output Dynamic Range (ODR) with little cell-to-cell variability. This repository allows to reproduce the results of this study. [Published paper is accessible here](https://doi.org/10.1186/s12859-023-05538-z).

## Setup

Make sure to install first the `slmcmc` package using `devtools`, and the `deSolve` and `lhs` packages.

```R
# install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/csb.ethz/slmcmc", build_vignettes = TRUE)

install.packages("deSolve")      #version used: 1.30
install.packages("lhs")          #version used: 1.1.3
```
Alternatively, you can set `build_vignettes = FALSE` if you do not wish to use the vignettes for documentation (Building the vignettes can take a few minutes).

## R session

For all the following, be sure to set your `R` session working directory to the WTC folder (current folder). This should be the case if you open the `WTC` project.

## Generate model file

Open the `R` project `WTC` found in this folder and run the script [model/generate_model.R](./model/generate_model.R). This should create a ".dll" file for simulation in the [model](./model) folder, from the C file [model/model_WTC_fastR.c](./model/model_WTC_fastR.c).

## Reproducing results
The following files will generate samples (or load them if a corresponding save file exists in [saves](./saves)).
Change the variable `save_res_mcmc` (in sample_indiv.R) or `save_res_mcmc_gamma` (other scripts) to a different name to re-generate samples, or delete the corresponding file in [saves](./saves). Change the value of `s_threshold`, the individual cost threshold, to either 6 or 3 to get the corresponding samples. Change the `variance_model` variable to select the structure of the covariance matrix for population sampling (`"identical"` for scalar covariance matrix, `"diagonal"` for diagonal covariance matrix). For the SLMCMC algorithm, change the `kappa_vec` variable to the desired vector of (integer) inverse temperatures.

* [sample_indiv.R](./sample_indiv.R) to get samples from the individual space. It will also produce the figures.
* [sample_pop_naive.R](./sample_pop_naive.R) to get samples from the population parameter space with the naive approach.
* [sample_pop_slmcmc.R](./sample_pop_slmcmc.R) to get samples from the population parameter space with the SLMCMC algorithm.
* [optimize_indiv_cost.R](./optimize_indiv_cost.R) to run an optimization procedure on the individual parameter space in order to find an individual parameter with minimum cost.
* [R_analysis/analysis.R](./R_analysis/analysis.R) to analyze the relationship between allowed variance and repression coefficients.

## Reproducing figures
The following files will produce figures from loaded files.

* [sample_indiv.R](./sample_indiv.R) to generate figures for the individual space (figure 3).
* [plots/plot_example_pop.R](./plots/plot_example_pop.R) to plot an example of a population (figure 2C).
* [plots/plot_res_mcmc_gamma.R](./plots/plot_res_mcmc_gamma.R) to generate figures for the population space (unused in paper).
* [plots/plot_combine_gamma.R](./plots/plot_combine_gamma.R) to combine results for different individual cost threshold in one figure (figure 4A, 4B, 5).
* [plots/plot_different_refs_gamma.R](./plots/plot_different_refs_gamma.R) to combine results for different design objectives in one figure (figure 6).
* [plots/plot_diagnostic_per_sample.R](./plots/plot_diagnostic_per_sample.R) plots a convergence diagnostic as a function of the number of samples in MCMC.
* [plots/plot_indiv_vs_fixed_CV.R](./plots/plot_indiv_vs_fixed_CV.R) plots a comparison of individual viable space against population viable space when the coefficient of variation (CV) is fixed.
* [plots/plot_k_vs_d_indiv.R](./plots/plot_k_vs_d_indiv.R) plots the relationship between production and degradation constants when all parameters are sampled.
* [plots/plot_subset_diag_sigma.R](./plots/plot_subset_diag_sigma.R) plots a subset of parameters' distributions when sampling separately the variance of parameters.

## Other files

* The folder [images](./images) contains images generated as above and saved as eps/svg files.
* The csv file [paramSpecsDilution.txt](./paramSpecsDilution.txt) is used to store parameter values and bounds.
* Other scripts [model/load_model.R](./model/load_model.R), [init_sample_pop.R](./init_sample_pop.R), [source_files.R](./source_files.R), are used by the scripts mentionned above.

Note that to run the sampling algorithms with the slightly different model where the Hill coefficient for TetR-Tup1 is increased, one should replace the csv file and loading model script with those currently named:
* [paramSpecsDilutionSepN.txt](./paramSpecsDilutionSepN.txt)
* [model/load_model2.R](./model/load_model2.R)