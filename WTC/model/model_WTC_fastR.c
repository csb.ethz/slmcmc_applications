/* file model_WTC_fastR.c */
/* This file is part of the following manuscript:
   Title:   Efficient design of synthetic gene circuits under cell-to-cell variability
   Authors: Baptiste Turpin, Eline Y. Bijman, Hans-Michael Kaltenbach, Joerg Stelling*
  
   *Corresponding authors
  
   (C) Copyright 2022 ETH Zurich, D-BSSE, Baptiste Turpin
  
   Licensed under the MIT license:
  
       http://www.opensource.org/licenses/mit-license.php */
#include <R.h>
static double parms[14];
#define aTcAdded parms[0]
#define ind_time parms[1]
#define d1 parms[2]
#define d2 parms[3]
#define d3 parms[4]
#define theta_alpha parms[5]
#define theta_beta parms[6]
#define ktet_alpha parms[7]
#define krnr_beta parms[8]
#define ktet_C parms[9]
#define kL parms[10]
#define Kd parms[11]
#define n parms[12]
#define nMperUnit parms[13]

/* initializer  */
void initmod(void (* odeparms)(int *, double *))
{int N=14;
odeparms(&N, parms);
}

/* Derivatives and 1 output variable */
void derivs (int *neq, double *t, double *y, double *ydot,double *yout, int *ip)
{
	if (ip[0] <1) error("nout should be at least 1");	
	//Variables
	double a, Ka, Delta, free_fraction, alpha_f, beta_f, g_factor, beta;
    a = 0;
	beta = krnr_beta / d2 * (1 - exp(-d2 * (*t)));
	if (*t>= ind_time) a = aTcAdded;
    Ka = nMperUnit / Kd;
    Delta = pow(1 + Ka*(y[0]+beta-a), 2) + 4*Ka*a;
    free_fraction = 0;
	if (y[0]+beta >= 1e-5) free_fraction = 0.5 - (1 + Ka*a - sqrt(Delta) )/(2*Ka*(y[0]+beta));
    alpha_f = y[0] * free_fraction;
    beta_f = beta * free_fraction;
    g_factor = (kL + (1-kL)*  1 /( 1 + pow(fmax(alpha_f,0)/theta_alpha,n) + pow(fmax(beta_f,0)/theta_beta, n) ));
    //ODE y = [alpha, beta, C]
    ydot[0] = ktet_alpha*g_factor  - d1* y[0];
    ydot[1] = ktet_C* g_factor - d3 * y[1];
	yout[0] = beta;
	/* yout[1] = a;
	yout[2] = free_fraction;
	yout[3] = sqrt(Delta);
	yout[4] = g_factor; */
	
	}
	
/* The Jacobian matrix */
/* void jac(int *neq, double *t, double *y, int *ml, int *mu,double *pd, int *nrowpd, double *yout, int *ip)
{
	//pd[0]               = -k1;
	//pd[1]               = k1;
	//pd[2]               = 0.0;
	//pd[(*nrowpd)]       = k2*y[2];
	//pd[(*nrowpd) + 1]   = -k2*y[2] - 2*k3*y[1];
	//pd[(*nrowpd) + 2]   = 2*k3*y[1];
	//pd[(*nrowpd)*2]     = k2*y[1];
	//pd[2*(*nrowpd) + 1] = -k2 * y[1];
	//pd[2*(*nrowpd) + 2] = 0.0;
	} */
	
	
/* END file mymod.c */