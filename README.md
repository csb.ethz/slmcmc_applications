# SLMCMC_applications

Apply the method of Stochastic-Likelihood Markov Chain Monte-Carlo (SLMCMC), implemented as the [slmcmc package](https://git.bsse.ethz.ch/csb/slmcmc), to a population sampling example.
[Published paper is accessible here](https://doi.org/10.1186/s12859-023-05538-z).

## Setup

Install first the `slmcmc` package using `devtools`

```R
# install.packages("devtools")
devtools::install_git(url = "https://gitlab.com/csb.ethz/slmcmc", build_vignettes = TRUE)
```

`deSolve` and `lhs` are required to run the WTC application.
```R
install.packages("deSolve")      #version used: 1.30
install.packages("lhs")          #version used: 1.1.3
```

## Applications

See [WTC](./WTC) for the Well-tempered controller application. 